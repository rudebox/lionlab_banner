<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M38QL5P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<header class="header clearfix" id="header"> 
  <div class="wrap hpad flex flex--center flex--justify header__container">

    <?php 

      // Logo markup
      $blog_name = get_bloginfo( 'name' );
      $logo  = ( is_front_page() ) ? '<h1 class="invisible">' . $blog_name . '</h1>' : '<p class="invisible">' . $blog_name . '</p>';
    ?>

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <?php echo $logo; ?>
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span>
      <img class="nav-toggle__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/burger.png" alt="burger-menu">
    </div>

    <nav class="nav" role="navigation">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
      </div>
    </nav>

  </div>
</header>

<div class="transition__pace"></div>

<div id="transition__wrapper">

<div class="transition__container" data-namespace="general">  