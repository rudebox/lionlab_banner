jQuery(document).ready(function($) {

    //menu toggle
    function toggleMenu() {
      $('.nav-toggle').click(function(e) {
        e.preventDefault();
        $('.nav--mobile').toggleClass('is-open');
        $('body').toggleClass('is-fixed');
      });
    }

    toggleMenu();

    //fade in menu-items
    function fadeMenu() {
      $('.nav-toggle').click(function() {
        $('.nav__item').each(function(index){        
            var delayNumber = index * 100;
            
            $(this).delay(delayNumber).queue(function(next) {
              $(this).toggleClass('is-animated');
              next();
            });  

        })
      }); 
    }

    fadeMenu();

    //when header is outside barba-container we need to update active class 
    $('.nav__item').on('click', function(e) {
      $('.nav__item').removeClass('is-active');
      $(this).addClass('is-active');
    });


    //close adblock modal
    $('.adblock-alert__inner').on('click', function() {
        $('.adblock-alert').removeClass('is-true');
        $('body').removeClass('is-fixed');
    });


    //delay cases
    function delay() {
      $('.cases__post, .banner-cat__item').each(function(index){
        var delayNumber = index * 100;
        $(this).attr('data-aos-delay', delayNumber).data('delayNumber');
      })
    }

    delay();


    //AOS init  
    AOS.init({
      duration: 600,
      once: true,
      easing: 'ease'
    })


  
    function tilt() {
      if ($('body').hasClass('home') || $('body').hasClass('blog')) {     
        var tilt = $('.js-tilt').tilt({
          maxTilt:        40,
          perspective:    1000,   // Transform perspective, the lower the more extreme the tilt gets.
          easing:         "cubic-bezier(.03,.98,.52,.99)",    // Easing on enter/exit.
          scale:          1,      // 2 = 200%, 1.5 = 150%, etc..
          speed:          300,    // Speed of the enter/exit transition.
          transition:     false,   // Set a transition on enter/exit.
          disableAxis:    null,   // What axis should be disabled. Can be X or Y.
          reset:          true,   // If the tilt effect has to be reset on exit.
          glare:          false,  // Enables glare effect
        })

        //destroy instance on mobile devices
        if ($(window).width() < 768) {
          tilt.tilt.destroy.call(tilt);
        }
      }
    }

    tilt();


    var getUrl = window.location;
    var getHomeUrl = getUrl.protocol + "//" + getUrl.host;

    //
    // Barba.js
    //
    Barba.Pjax.Dom.wrapperId = 'transition__wrapper';
    Barba.Pjax.Dom.containerClass = 'transition__container';
    Barba.Pjax.ignoreClassLink = 'no-ajax';

    var general = Barba.BaseView.extend({
      namespace: 'general',
      onEnter: function() {
        // The new Container is ready and attached to the DOM.
        
      },
      onEnterCompleted: function() {
        // The Transition has just finished.
        $('.transition__pace').removeClass('is-active');
        $('.banner__hero').addClass('is-active');
        $('.hero').addClass('is-active');
      },
      onLeave: function() {
        // A new Transition toward a new page has just started.
        $('.transition__pace').addClass('is-active');
        $('.hero').addClass('is-active');
      },
      onLeaveCompleted: function() {
        // The Container has just been removed from the DOM.

      }
    });

    general.init();

    Barba.Pjax.start();

    Barba.Prefetch.init(); 

    Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

    Barba.Pjax.preventCheck = function(evt, element) {
      if (!Barba.Pjax.originalPreventCheck(evt, element)) {
        return false;
      }

      //outcomment if you want to prevent barba when logged in
      // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
      //   return false;  
      // }; } 

      // No need to check for element.href -
      // originalPreventCheck does this for us! (and more!)
      if (/.pdf/.test(element.href.toLowerCase())) {
        return false;
      } else if (/edit/.test(element.href.toLowerCase())) {
        return false;
      } else if (/wp-admin/.test(element.href.toLowerCase())) {
        return false;
      }

      return true;
    };

    var FadeTransition = Barba.BaseTransition.extend({
    start: function() {
    /**
    * This function is automatically called as soon the Transition starts
    * this.newContainerLoading is a Promise for the loading of the new container
    * (Barba.js also comes with an handy Promise polyfill!)
    */
    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
    .all([this.newContainerLoading, this.fadeOut()])
    .then(this.fadeIn.bind(this));
    },
    fadeOut: function() {
    /**
    * this.oldContainer is the HTMLElement of the old Container
    */
      return $(this.oldContainer).animate({ opacity: 0 }).promise();
    },
    fadeIn: function() {
      /**
      * this.newContainer is the HTMLElement of the new Container
      * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
      * Please note, newContainer is available just after newContainerLoading is resolved!
      */
      var _this = this;
      var $newContainer = $(this.newContainer);

      $(this.oldContainer).hide();
      $(window).scrollTop(0); // scroll to top here

      $newContainer.css({
        visibility : 'visible',
        opacity : 1,
      });

      $newContainer.animate({
        opacity: 1,

      }, 400, function() { 
        /**
        * Do not forget to call .done() as soon your transition is finished!
        * .done() will automatically remove from the DOM the old Container
        */
        _this.done();

      });
      }

    });
    /**
    * Next step, you have to tell Barba to use the new Transition
    */
    Barba.Pjax.getTransition = function() {
    /**
    * Here you can use your own logic!
    * For example you can use different Transition based on the current page or link...
    */
    return FadeTransition;

    };

    Barba.Dispatcher.on('initStateChange', function() {

      toggleMenu();

      fadeMenu();

      $('body').removeClass('is-fixed');
      $('.nav--mobile').removeClass('is-open');

      //these inline scripts for GF is needed in order for conditional logic to work
      jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 3) {gf_global["number_formats"][3] = {"1":{"price":false,"value":false},"2":{"price":false,"value":false},"4":{"price":false,"value":false},"3":{"price":false,"value":false},"6":{"price":false,"value":false},"8":{"price":false,"value":false},"9":{"price":false,"value":false},"10":{"price":false,"value":false}};if(window['jQuery']){if(!window['gf_form_conditional_logic'])window['gf_form_conditional_logic'] = new Array();window['gf_form_conditional_logic'][3] = { logic: { 8: {"field":{"actionType":"show","logicType":"all","rules":[{"fieldId":"6","operator":"isnot","value":"Jeg er ikke en robot *"}]},"nextButton":null,"section":null},9: {"field":{"actionType":"show","logicType":"all","rules":[{"fieldId":"6","operator":"is","value":"Jeg er ikke en robot *"}]},"nextButton":null,"section":null} }, dependents: { 8: [8],9: [9] }, animation: 0, defaults: [], fields: {"1":[],"2":[],"4":[],"3":[],"6":[8,9],"8":[],"9":[],"10":[]} }; if(!window['gf_number_format'])window['gf_number_format'] = 'decimal_comma';jQuery(document).ready(function(){gf_apply_rules(3, [8,9], true);jQuery('#gform_wrapper_3').show();jQuery(document).trigger('gform_post_conditional_logic', [3, null, true]);} );} if(typeof Placeholders != 'undefined'){
      Placeholders.enable();
      }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );

      jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [3, 1]) } ); 

      //Update tag manager
      if (typeof ga !== 'function' || Barba.HistoryManager.history.length <= 1) {
        return;
      }

      var trackers = ga.getAll();
      trackers.forEach(function(tracker) {
        ga(tracker.get('name') + '.send', 'pageview', window.location.pathname);
      });

    });

    /**
    * GET WP body classes
    */
    Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

      // html head parser borrowed from jquery pjax
      var $newPageHead = $( '<head />' ).html(
          $.parseHTML(
              newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
            , document
            , true
          )
      );
      var headTags = [
          "meta[name='keywords']"
        , "meta[name='description']"
        , "meta[property^='og']"
        , "meta[name^='twitter']"
        , "meta[itemprop]"
        , "link[itemprop]"
        , "link[rel='prev']"
        , "link[rel='next']"
        , "link[rel='canonical']"
      ].join(',');
      $( 'head' ).find( headTags ).remove(); // Remove current head tags
      $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head 


      //update body classes
      var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
      var bodyClasses = $(response).filter('notbody').attr('class')
      $('body').attr('class', bodyClasses);

      //reinit delay
      delay();

      //reinit tilt
      tilt();

      //reinit menu
      toggleMenu();

      //reinit fade menu
      fadeMenu();

      //these inline scripts for GF is needed in order for conditional logic to work
      jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 3) {gf_global["number_formats"][3] = {"1":{"price":false,"value":false},"2":{"price":false,"value":false},"4":{"price":false,"value":false},"3":{"price":false,"value":false},"6":{"price":false,"value":false},"8":{"price":false,"value":false},"9":{"price":false,"value":false},"10":{"price":false,"value":false}};if(window['jQuery']){if(!window['gf_form_conditional_logic'])window['gf_form_conditional_logic'] = new Array();window['gf_form_conditional_logic'][3] = { logic: { 8: {"field":{"actionType":"show","logicType":"all","rules":[{"fieldId":"6","operator":"isnot","value":"Jeg er ikke en robot *"}]},"nextButton":null,"section":null},9: {"field":{"actionType":"show","logicType":"all","rules":[{"fieldId":"6","operator":"is","value":"Jeg er ikke en robot *"}]},"nextButton":null,"section":null} }, dependents: { 8: [8],9: [9] }, animation: 0, defaults: [], fields: {"1":[],"2":[],"4":[],"3":[],"6":[8,9],"8":[],"9":[],"10":[]} }; if(!window['gf_number_format'])window['gf_number_format'] = 'decimal_comma';jQuery(document).ready(function(){gf_apply_rules(3, [8,9], true);jQuery('#gform_wrapper_3').show();jQuery(document).trigger('gform_post_conditional_logic', [3, null, true]);} );} if(typeof Placeholders != 'undefined'){
      Placeholders.enable();
      }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );

      jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [3, 1]) } ); 


    });

    Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {

    });

});
