<?php 
  if ( is_tax() ) {
    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $title = $term->name;
  } elseif ( is_category() ) {
    $title = single_cat_title( '', false );
  } elseif ( is_archive() ) {
    $title = post_type_archive_title( '', false );
  } elseif ( is_404() ) {
    $title = __('Siden kunne ikke findes', 'lionlab');
  } elseif ( is_search() ) {
    $title = __('Søgeresultat', 'lionlab');
  } else {
    $id = (is_home()) ? get_option('page_for_posts') : $post->ID;
    $title = get_proper_title($id);
  }

  //hero field group
  $img = get_field('page_img');
  $text = get_field('page_text');
  $link = get_field('page_link');
?>


<section class="hero padding--both">
  <div class="wrap clearfix hpad">
      <div class="row hero__row">

        <div class="hero__text col-sm-10">          
          <h1 data-aos="fade-up" data-aos-delay="1200" class="hero__title"><?php echo esc_html($title); ?></h1>
          <div data-aos="fade-up" data-aos-delay="1200">
            <?php echo $text; ?>
          </div>
        </div>

      </div>
  </div>
</section>
