<?php if (!is_single() ) : ?>
<footer id="footer" class="footer purple--bg clearfix">
	<div class="wrap hpad">
		<div class="footer__item center">
			<h4 class="footer__title"><?php echo the_field('footer_title', 'options'); ?></h4>
			<?php echo the_field('footer_info', 'options'); ?>
	  	</div> 
  	</div>
</footer>
<?php endif; ?>

</div>
</div>


<div class="adblock-alert">
	<div class="adblock-alert__inner">
		<span class="adblock-alert__close"><i class="fa fa-close"></i></span>
		<h2 class="center">Adblock</h2>
		<div class="center">
			<p>Da vores banner er en live gengivelse af dem du vil se på nettet,<br> er det nødvendigt du slår Adblock fra for at kunne se dem.</p>
			<div class="btn btn--gradient">OK</div>	
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
