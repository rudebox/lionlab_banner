<?php $icon = get_field('login_icon', 'options'); ?>

<div class="toolbar gray--bg">
	<div class="wrap hpad toolbar__container flex flex--justify">
		<div class="toolbar__item toolbar__item--compress">
			<button class="toolbar__item btn btn--compress bx-shadow--purple"><i class="fa fa-compress"></i></button>
		</div>

		<div class="toolbar__wrap flex">
			<div class="toolbar__search">
				<form method="get" id="searchform" autocomplete="off" action="<?php bloginfo('url'); ?>/">
				  <div class="toolbar__search--wrapper">
				    <input type="text" value="<?php the_search_query(); ?>" placeholder="Indtast søgeord..." name="s" id="s" />
				    <button type="submit" class="fa fa-search btn btn--search bx-shadow--purple"></button>
				  </div>
				</form>
			</div>
<!-- 
			<div class="toolbar__item">
				<a class="btn btn--login bx-shadow--purple" href="<?php echo get_template_directory_uri()?>/kundelogin/"><i class="fa fa-user"></i></a> 
			</div> -->
		</div>
	</div>
</div>