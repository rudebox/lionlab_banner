<?php
  //cat banner repeater field group
  
  if (have_rows('cat_box') ) :
?>


<section class="banner-cat padding--top">
  <div class="wrap clearfix hpad">
    <div class="row flex flex--wrap">
      <?php while (have_rows('cat_box') ) : the_row(); 
          $title = get_sub_field('cat_title');
          $img = get_sub_field('cat_img');
          $text = get_sub_field('cat_text');
      ?>

        <div data-aos="fade-up" class="col-sm-4 banner-cat__item bx-shadow bx-shadow--purple white--bg">
          <?php if ($img) : ?>
          <div class="banner-cat__icon bx-shadow--purple">
            <?php echo file_get_contents($img['url']); ?>
          </div>
          <?php endif; ?>
          <h4><?php echo $title; ?></h4>
          <?php echo $text; ?>
        </div>

      <?php endwhile; ?>

    </div>
  </div>
</section>
<?php endif; ?>