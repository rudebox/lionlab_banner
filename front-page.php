<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/hero'); ?>
 
  <?php get_template_part('parts/content', 'layouts'); ?>

	<section class="cases padding--both">
		<div class="wrap hpad clearfix">
			<h2 class="cases__header">Cases</h2><br>
			<div class="row flex flex--wrap">

			  <?php 
				// Query Arguments
				$args = array(
					'posts_per_page' => 6,
					'order' => 'ASC',
					'category__not_in' => 8
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>


				<?php 
					//post thumbnail
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );

					//colorpicker
					$colorpicker = get_field('colorpicker');
				?>

				<a data-aos="fade-up" data-tilt class="cases__post cases__post--home bx-shadow bx-shadow--purple col-sm-3 js-tilt" href="<?php echo the_permalink(); ?>">
					<div class="cases__post--header" style="background-color: <?php echo esc_attr($colorpicker); ?>";>
						<?php if ($thumb) : ?>
								<img class="cases__post--thumb" src="<?php echo $thumb['0']; ?>" alt="<?php echo $thumb['alt']; ?>">
							<?php endif; ?>
					</div>
					<h3 class="cases__post--title"><?php the_title(); ?></h3>

					<?php the_excerpt(); ?>	

					<div class="cases__post--meta">
						<span class="cases__cat">
							<?php 
							    foreach((get_the_category()) as $category) {
							        echo $category->name."<br>";
							    }
		    				?>
    					</span>
					</div>
				</a>		

				<?php wp_reset_postdata(); ?>
				<?php endwhile; endif; ?>
			</div>
			<div class="center">
				<a class="btn btn--gradient cases__btn" href="/cases">Se alle cases</a>
			</div>
		</div>
	</section>

</main>

<?php get_template_part('parts/footer'); ?>
