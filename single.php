<?php get_template_part('parts/header'); the_post(); ?>

<main>
  
<section class="banner__hero">
  <div class="wrap hpad padding--both">
    <div class="row flex flex--wrap flex--justify clear banner__hero--row">
      <div class="col-sm-6 banner__text">
        <h2 data-aos="fade-up" data-aos-delay="1200" class="banner__title h3">HTML5 Display</h2> 
        <h1 data-aos="fade-up" data-aos-delay="1200" class="banner__client"><?php echo the_title(); ?></h1>
      </div>

      <div class="col-sm-3 banner__share">
        <h4 data-aos="fade-up" data-aos-delay="1800" class="banner__share--title">Del med dine "venner"</h4>

        <div class="banner__share--wrap">
          <a href="https://www.facebook.com/sharer/sharer.php?u=#<?php echo $_SERVER['REQUEST_URI']; ?>" data-aos="fade-in" data-aos-delay="1900" class="banner__share banner__share--link">
          <i class="fa fa-facebook"></i>
          </a>
          <a href="https://twitter.com/share#<?php echo $_SERVER['REQUEST_URI']; ?>" data-aos="fade-in" data-aos-delay="2000" class="banner__share banner__share--link">
          <i class="fa fa-twitter"></i>
          </a>
          <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://banner.lionlab.dk<?php echo $_SERVER['REQUEST_URI']; ?>" data-aos="fade-in" data-aos-delay="2100" class="banner__share banner__share--link">
          <i class="fa fa-linkedin"></i>
          </a>
        </div>

      </div>
    </div>
  </div>
</section>  

<div class="wrap hpad banner__container padding--both">
  <div class="row-fluid">
  <div class="banner__header">
    <span></span>
    <span></span>
    <span></span>
    <div class="hidden-sm" id="iframecontainer1" >
    <iframe  src="<?php echo the_field('banner_top'); ?>" height="90" width="728" style="border:none;"></iframe>
  </div> 
</div>





<div class="container main-container">
  <div class="row banner__row flex flex--wrap">

    <div class="col-sm-8">
      <div class="row flex flex--center">
      <div class="col-sm-7">

        <?php 
          $banner_320x320 = get_field('banner_mobile');
        ?>

        <div id="iframecontainer2" >
          <?php if ($banner_320x320) : ?>
            <iframe src="<?php echo esc_url($banner_320x320); ?>" height="320" width="320" style="border:none;"></iframe> 
          <?php endif; ?>
        </div>

      </div>

      <div class="col-sm-5 hidden-sm">
        <div class="banner__placeholder banner__placeholder--heading gray-dark--bg"></div>
        <div class="banner__placeholder banner__placeholder--sentence "></div>
        <div class="banner__placeholder banner__placeholder--sentence "></div>
        <br>

        <div class="banner__placeholder banner__placeholder--heading gray-dark--bg banner__placeholder--heading--smaller"></div>
        <div class="banner__placeholder banner__placeholder--sentence banner__placeholder--sentence--last banner__placeholder--sentence--smaller"></div>
        <div class="banner__placeholder banner__placeholder--sentence banner__placeholder--sentence--last banner__placeholder--sentence--smaller"></div>
        <div class="banner__placeholder banner__placeholder--sentence banner__placeholder--sentence--last banner__placeholder--sentence--smaller"></div>
        </div>
      </div>    
    
      <div class="banner__placeholder banner__placeholder--sentence gray-dark--bg hidden-sm"></div>


      <div class="row">
        <div class="col-sm-4 hidden-sm">
          <div class="banner__placeholder banner__placeholder--paragraph"></div>
          <div class="banner__placeholder banner__placeholder--sentence gray-dark--bg "></div>
          <br/>
          <div class="banner__placeholder banner__placeholder--sentence"></div>
        </div>

        <div class="col-sm-4 hidden-sm">
          <div class="banner__placeholder banner__placeholder--paragraph"></div>
          <div class="banner__placeholder banner__placeholder--sentence gray-dark--bg"></div>
          <br/>
          <div class="banner__placeholder banner__placeholder--sentence"></div>
        </div>

        <div class="col-sm-4">
          <div class="banner__placeholder banner__placeholder--paragraph"></div>
          <div class="banner__placeholder banner__placeholder--sentence gray-dark--bg"></div>
          <br/>
          <div class="banner__placeholder banner__placeholder--sentence"></div>
        </div>
      </div>   

    </div>

    <div class="col-sm-4">
      <?php 
        $banner_600x160 = get_field('banner_sidebar');
        $banner_600x300 = get_field('banner_sidebar_wide'); 
      ?>

      <?php if ($banner_600x160) : ?>
        <iframe src="<?php echo esc_url($banner_600x160); ?>" height="600" width="160" style="border:none;"></iframe> 
      <?php endif; ?>

      <?php if ($banner_600x300) : ?>
        <iframe src="<?php echo esc_url($banner_600x300); ?>" height="600" width="300" style="border:none;"></iframe> 
      <?php endif; ?>
    </div>

  </div>
</div>

<div class="container">
  <div class="row flex flex--wrap">

    <div class="col-sm-4 hidden-sm">
      <div class="banner__placeholder  banner__placeholder--paragraph">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pictureicon.svg"  height="120" class="center" alt="Your Picture here">
      </div>
      <div class="banner__placeholder banner__placeholder--heading gray-dark--bg" style="float:left;"></div>
      <br></br> 
      <div class="banner__placeholder banner__placeholder--sentence"></div>                                
      <div class="banner__placeholder banner__placeholder--sentence"></div>
    </div>

    <div class="col-sm-4 hidden-sm">

      <?php 
        $banner_300x250 = get_field('banner_rectangular');
      ?>

      <?php if ($banner_300x250) : ?>
        <iframe src="<?php echo the_field('banner_rectangular'); ?>" height="250" width="300" style="border:none;"></iframe>
      <?php endif; ?>

      <?php if (!$banner_300x250) : ?>
        <div class="banner__placeholder banner__placeholder--paragraph">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pictureicon.svg"  height="120" class="center" alt="Your Picture here">
        </div>
        <div class="banner__placeholder banner__placeholder--heading gray-dark--bg" style="float:left;"></div>
        <br></br>
        <div class="banner__placeholder banner__placeholder--sentence"></div>                                
        <div class="banner__placeholder banner__placeholder--sentence"></div>
      <?php endif; ?>

    </div>

    <div class="col-sm-4 hidden-sm">
      <div class="banner__placeholder banner__placeholder--paragraph">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pictureicon.svg"  height="120" class="center" alt="Your Picture here">
      </div>
      <div class="banner__placeholder banner__placeholder--heading gray-dark--bg" style="float:left;"></div>
      <br></br>
      <div class="banner__placeholder banner__placeholder--sentence"></div>                                
      <div class="banner__placeholder banner__placeholder--sentence"></div>
    </div>

    </div>

  </div>            
</div>

</main>

<?php  
 $next_post = get_next_post();
 $previous_post = get_previous_post();

 if (!empty( $next_post )):
?>
<a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>"  data-aos="bg-change" data-aos-delay="300" data-aos-duration="1200" class="banner__nav">
  <div class="wrap clearfix center" data-aos="fade-up" data-aos-delay="600">
      <p>Næste case</p>
      <span class="banner__nav--link"><h2><?php echo esc_attr( $next_post->post_title ); ?></h2></span>
  </div>
</a>
<?php endif; ?>

<?php if (empty( $next_post )): ?>
<a href="<?php echo esc_url( get_permalink( $previous_post->ID ) ); ?>" data-aos="bg-change" data-aos-delay="300" data-aos-duration="1200" class="banner__nav">
  <div class="wrap clearfix center" data-aos="fade-up" data-aos-delay="600">
      <p>Forrige case</p>
      <span class="banner__nav--link"><h2><?php echo esc_attr( $previous_post->post_title ); ?></h2></span>
  </div>
</a>
<?php endif; ?>

<?php get_template_part('parts/footer'); ?>